global _start
section .data
codes: db      '0123456789ABCDEF'
section .text
print_hex8:                     ;    rdi = char
  add rdi, codes
  mov rsi, rdi
  mov rax, 1
  mov rdi, 1
  mov rdx, 1
  syscall
  ret

print_hex64:                    ; rdi = contents
    mov rcx, 64
.loop:
    push rdi
    sub rcx, 4
    sar rdi, cl
    and rdi, 0xf
    push rcx
    call print_hex8
    pop rcx
    pop rdi
    test rcx, rcx
    jnz .loop
  ret

exit:
  mov rax, 60
  mov rdi, 0
  syscall

_start:
  mov rdi, 1122334455667788h
  call print_hex64
  call exit
